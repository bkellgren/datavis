/**
 * TDB.demo - TempoDB Data Visualization Demo
 * @author Brett Kellgren
 *
 * Dependencies:
 *
 * jQuery - <script type="text/javascript" src="js/vendor/jquery-1.7.2.min.js"></script>
 * jQuery UI - <script src="js/vendor/jquery-ui-1.8.15.min.js"></script>
 * D3.js - <script src="js/vendor/d3.v2.js"></script>
 * Moment.js - <script src="js/vendor/moment.min.js"></script>
 * Rickshaw.js - <script src="js/vendor/rickshaw.min.js"></script>
 * extensions.js - <script src="js/vendor/extensions.js"></script>
 *
 * jQuery UI css - <link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css">
 * Rickshaw graph.css - <link type="text/css" rel="stylesheet" href="css/graph.css">
 * Rickshaw detail.css - <link type="text/css" rel="stylesheet" href="css/detail.css">
 * Rickshaw legend.css - <link type="text/css" rel="stylesheet" href="css/legend.css">
 * TDB demo.css - <link rel="stylesheet" href="css/demo.css">
 *
 * Usage:
 * TDB.demo.init('data/data.csv');
 *
 * Required Markup:
 *
    <form id="side_panel">
        <h4>Data Points</h4>
        <ul class="muted">
            <li>click the &#10004; to toggle on/off</li>
            <li>click <span class="label">label</span> to view alone</li>
            <li>drag to reorder</li>
        </ul>
        <section><div id="legend"><img class="loading" src="img/loading.gif" alt="loading..."></div></div></section>
        <section>
            <div id="renderer_form" class="toggler">
                <input type="radio" name="renderer" id="area" value="area" checked>
                <label for="area">area</label>
                <input type="radio" name="renderer" id="bar" value="bar">
                <label for="bar">bar</label>
                <input type="radio" name="renderer" id="line" value="line">
                <label for="line">line</label>
                <input type="radio" name="renderer" id="scatter" value="scatterplot">
                <label for="scatter">scatter</label>
            </div>
        </section>
        <section>
            <div id="offset_form">
                <label for="stack">
                    <input type="radio" name="offset" id="stack" value="zero" checked>
                    <span>stack</span>
                </label>
                <label for="stream">
                    <input type="radio" name="offset" id="stream" value="wiggle">
                    <span>stream</span>
                </label>
                <label for="pct">
                    <input type="radio" name="offset" id="pct" value="expand">
                    <span>pct</span>
                </label>
                <label for="value">
                    <input type="radio" name="offset" id="value" value="value">
                    <span>value</span>
                </label>
            </div>
            <div id="interpolation_form">
                <label for="cardinal">
                    <input type="radio" name="interpolation" id="cardinal" value="cardinal" checked>
                    <span>cardinal</span>
                </label>
                <label for="linear">
                    <input type="radio" name="interpolation" id="linear" value="linear">
                    <span>linear</span>
                </label>
                <label for="step">
                    <input type="radio" name="interpolation" id="step" value="step-after">
                    <span>step</span>
                </label>
            </div>
        </section>
        <section>
            <h6>Smoothing</h6>
            <div id="smoother"></div>
        </section>
        <section></section>
    </form>
    <div id="chart_container">
        <div id="chart"><img class="loading" src="img/loading.gif" alt="loading..."></div></div>
        <div id="timeline"></div>
        <div id="slider"></div>
    </div>
 *
 */


var TDB = TDB || {};

TDB.demo = (function($) {

    /*  Properties ______________________________________________________________ */

    var seriesData = [];


    /*  Methods _________________________________________________________________ */

    function init(csvPath) {

        d3.csv(csvPath, function(rows) {
            rows.forEach(function(o) {
                var iter = 0,
                    dt;
                for(prop in o){
                    // First column should be a date/time in the following format
                    if(!iter){
                        // convert string date to Date and convert to unix via moment.js
                        o[prop] = parseInt(moment(o[prop], "YYYY-MM-DD-HH").unix());
                        dt = o[prop];
                    }else{
                        o[prop] = parseFloat(o[prop]);
                        // ensure array exists, if not create it
                        // http://stackoverflow.com/questions/1961528/javascript-check-if-array-exist-if-not-create-it/1961653#1961653
                        seriesData[iter-1] = Object.prototype.toString.call(seriesData[iter-1]) == "[object Array]" ? seriesData[iter-1] : [];
                        // save series data
                        seriesData[iter-1].push({x: dt, y: o[prop]});
                    }
                    iter++;
                }
            });
            render(seriesData, rows[0]);
        });

        // selectors
        $loading = $('.loading');
    }

    function render(seriesData, row) {
        // settings
        var palette = new Rickshaw.Color.Palette( { scheme: 'colorwheel' }),
            ticksTreatment = 'glow',

            // iterators
            iter = 1, // start with 1 due to date as first col
            labelIter = 0,

            // arrays
            seriesUI = [],
            labels = ['null']; // build array of labels, set first (date label) to 'null'

        // build array of label names

        for(name in row){
            if(labelIter){ // skip first label as that is the date label
               labels.push(name);
            }
            labelIter++;
        }

        // build array of RickShaw series
        //
        for(series in seriesData){
            seriesUI.push({
                color: palette.color(),
                data: seriesData[series],
                name: labels[iter]
            });
            iter++;
        }

        // setup graph

        var graph = new Rickshaw.Graph( {
            element: document.getElementById("chart"),
            width: 900,
            height: 500,
            renderer: 'area',
            stroke: true,
            series: seriesUI
        });

        // graph.render callback

        graph.updateCallbacks = [function(){
                $loading.remove();
            }]

        graph.render();

        // let's spice it up with some additional RickShaw features

        var slider = new Rickshaw.Graph.RangeSlider( {
            graph: graph,
            element: $('#slider')
        });

        var hoverDetail = new Rickshaw.Graph.HoverDetail( {
            graph: graph
        });

        var annotator = new Rickshaw.Graph.Annotate( {
            graph: graph,
            element: document.getElementById('timeline')
        });

        var legend = new Rickshaw.Graph.Legend( {
            graph: graph,
            element: document.getElementById('legend')
        });

        var shelving = new Rickshaw.Graph.Behavior.Series.Toggle( {
            graph: graph,
            legend: legend
        });

        var order = new Rickshaw.Graph.Behavior.Series.Order( {
            graph: graph,
            legend: legend
        });

        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight( {
            graph: graph,
            legend: legend
        });

        var smoother = new Rickshaw.Graph.Smoother( {
            graph: graph,
            element: $('#smoother')
        });

        // Lets default to some smoothing before rendering x/y
        // so we don't melt everyone's cpu right off the bat

        $('#smoother').slider( "option", "value", 50 );
        smoother.setScale(50);

        var xAxis = new Rickshaw.Graph.Axis.Time( {
            graph: graph,
            ticksTreatment: ticksTreatment
        });

        xAxis.render();

        var yAxis = new Rickshaw.Graph.Axis.Y( {
            graph: graph,
            tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
            ticksTreatment: ticksTreatment
        });

        yAxis.render();

        var controls = new RenderControls( {
            element: document.querySelector('form'),
            graph: graph
        });

        // Add some sample annotations

        annotator.add(1294628400, 'Sample Annotation');
        annotator.add(1296097200, 'Another Annotation');

    }

    /* Helpers ________________________________________________________________________ */

    /**
     * Expose public API
     */
    return {
        init: init
    };

})(jQuery);